# import the necessary packages
import numpy as np
import argparse
import cv2

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", help="path to the image")
args = vars(ap.parse_args())

# load the image
image1 = cv2.imread(args["image"])
image= cv2.resize(image1, (300, 200))

# define the list of boundaries
boundaries = [
    ([102, 102, 0], [255, 255, 153])
]

# loop over the boundaries
for (lower, upper) in boundaries:
    # create NumPy arrays from the boundaries
    lower = np.array(lower, dtype="uint8")
    upper = np.array(upper, dtype="uint8")

    # find the colors within the specified boundaries and apply
    # the mask
    mask = cv2.inRange(image, lower, upper)
    output = cv2.bitwise_and(image, image, mask=mask)

    ratio = cv2.countNonZero(mask) / (image.size / 3)
    print('green pixel percentage:', np.round(ratio * 100, 2))
