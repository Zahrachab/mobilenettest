import cv2
import pytesseract
import numpy as np
import sys

def get_code(path):
    data = []
    originalImage = cv2.imread(path)

    slicedImage = originalImage[0:150, 875:1140]
    gray = cv2.cvtColor(slicedImage, cv2.COLOR_BGR2GRAY)

    ret, thresh = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY_INV)

    kernel = np.ones((2, 1), np.uint8)
    img = cv2.cvtColor(slicedImage, cv2.COLOR_BGR2GRAY)
    img = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                cv2.THRESH_BINARY, 11, 5)
    img = cv2.erode(img, kernel, iterations=1)
    res2 = pytesseract.image_to_string(img)
    data.append(res2)
    return data


res=get_code(sys.argv[1])
data = res[0].replace("{ ", "", 1)
data1 = data.replace("'", "", 1)
data2 = data1.replace("a", "o", 1)
data3 = data2.replace("9", "e", 1)
print(data3.replace("\n", "", 3))
sys.stdout.flush()

