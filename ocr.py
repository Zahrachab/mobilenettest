import pytesseract
from PIL import Image
import cv2
import numpy as np

img = cv2.imread('ecran.png',cv2.IMREAD_COLOR) #Open the image from which charectors has to be recognized
#img = cv2.resize(img, (620,480) ) #resize the image if required
image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

kernel = np.ones((1,1), np.uint8)
image= cv2.dilate(image, kernel, iterations=1)
image = cv2.erode(image, kernel, iterations=1)
original = pytesseract.image_to_string(image)
#eval = (pytesseract.image_to_data(gray, lang=None, config='', nice=0) ) #get confidence level if required
#print(pytesseract.image_to_boxes(gray))

print (original)