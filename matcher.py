import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt
img1 = cv.imread('examples/erreur1.jpg',cv.IMREAD_GRAYSCALE)          # queryImage
img2 = cv.imread('examples/ecran1.jpg',cv.IMREAD_GRAYSCALE) # trainImage
# Initiate ORB detector
brisk = cv.BRISK_create(10)

(kp1,des1) = brisk.detectAndCompute(img1,None)
# find the keypoints and descriptors with ORB
kp2, des2 = brisk.detectAndCompute(img2,None)

# create BFMatcher object
bf = cv.BFMatcher(cv.NORM_HAMMING, crossCheck=True)
# Match descriptors.
matches = bf.match(des1,des2)
# Sort them in the order of their distance.
matches = sorted(matches, key = lambda x:x.distance)
print(matches)
# Draw first 10 matches.
img3 = cv.drawMatches(img1,kp1,img2,kp2,matches[:100],None,flags=cv.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)
plt.imshow(img3),plt.show()
print(des1)

cv.waitKey(0)