# import the necessary packages
from skimage.measure import compare_ssim
import argparse
import imutils
import cv2

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

# load the two input images
image_orig = cv2.imread("ecran.png")

image_mod = cv2.imread("ecran2.png")

resized_orig = cv2.resize(image_orig, (300, 200))
resized_mod = cv2.resize(image_mod, (300, 200))

plt.imshow(resized_orig)
plt.imshow(resized_mod)

# convert the images to grayscale
gray_orig = cv2.cvtColor(resized_orig, cv2.COLOR_BGR2GRAY)
gray_mod = cv2.cvtColor(resized_mod, cv2.COLOR_BGR2GRAY)

# compute the Structural Similarity Index (SSIM) between the two
# images, ensuring that the difference image is returned
(score, diff) = compare_ssim(gray_orig, gray_mod, full=True)

diff = (diff * 255).astype("uint8")

print("Structural Similarity Index: {}".format(score))


