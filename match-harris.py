import numpy as np
import cv2
import imutils
from matplotlib import pyplot as plt

filename = 'examples/erreur1.jpg'
img1 = cv2.imread(filename)
img1 = imutils.resize(img1, height=300)
gray = cv2.cvtColor(img1,cv2.COLOR_BGR2GRAY)

gray = np.float32(gray)
ds1 = cv2.cornerHarris(gray,2,3,0.04)
#result is dilated for marking the corners, not important
ds1 = cv2.dilate(ds1,None)

filename2 = 'examples/ecran1.jpg'
img2 = cv2.imread(filename2)
img2 = imutils.resize(img2, height=300)
gray = cv2.cvtColor(img2,cv2.COLOR_BGR2GRAY)
gray = np.float32(gray)
ds2 = cv2.cornerHarris(gray,2,3,0.04)
#result is dilated for marking the corners, not important
ds2 = cv2.dilate(ds2,None)

# Initiate SIFT detector
orb = cv2.ORB()

# create BFMatcher object
bf = cv2.BFMatcher(cv2.ORB_HARRIS_SCORE, crossCheck=True)

# Match descriptors.
matches = bf.match(ds1,ds2)

# Sort them in the order of their distance.
matches = sorted(matches, key = lambda x:x.distance)

print(matches)
