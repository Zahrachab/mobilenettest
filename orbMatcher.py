import cv2
import imutils
import numpy as np


rectKernel = cv2.getStructuringElement(cv2.MORPH_RECT, (20, 20))
sqKernel = cv2.getStructuringElement(cv2.MORPH_RECT, (20, 20))


img1 = cv2.imread("examples/erreur1.jpg", cv2.IMREAD_GRAYSCALE)
img1 = imutils.resize(img1, height=300)
img1 = cv2.GaussianBlur(img1, (3, 3), 0)


img2 = cv2.imread("examples/ecran1.jpg", cv2.IMREAD_GRAYSCALE)
img2 = imutils.resize(img2, height=300)
img2 = cv2.GaussianBlur(img2, (3, 3), 0)


# ORB Detector
orb = cv2.BRISK_create()
kp1, des1 = orb.detectAndCompute(img1, None)
kp2, des2 = orb.detectAndCompute(img2, None)
# Brute Force Matching
bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
matches = bf.match(des1, des2)
matches = sorted(matches, key=lambda x: x.distance)
matching_result = cv2.drawMatches(img1, kp1, img2, kp2, matches[:10], None, flags=2)
print('ORB Matching Results')
print('*******************************')
print('# Keypoints 1:                        \t', len(kp1))
print('# Keypoints 2:                        \t', len(kp2))
print('# Matches:                            \t', len(matches))

cv2.imshow("Img1", img1)
cv2.imshow("Img2", img2)
cv2.imshow("Matching result", matching_result)
cv2.waitKey(0)
cv2.destroyAllWindows()
