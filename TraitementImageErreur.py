# USAGE
# python detect_mrz.py --images examples

# import the necessary packages
import numpy as np
import imutils
import cv2



# initialize a rectangular and square structuring kernel
rectKernel = cv2.getStructuringElement(cv2.MORPH_RECT, (40, 40))
sqKernel = cv2.getStructuringElement(cv2.MORPH_RECT, (40, 40))

# load the image, resize it, and convert it to grayscale
image = cv2.imread("examples/erreur1.jpg")
image = imutils.resize(image, height=600)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# smooth the image using a 3x3 Gaussian, then apply the blackhat
# morphological operator to find dark regions on a light background
gray = cv2.GaussianBlur(gray, (3, 3), 0)
cv2.imshow("gaussian filter", gray)
blackhat = cv2.morphologyEx(gray, cv2.MORPH_TOPHAT, rectKernel)
cv2.imshow("morphological", blackhat)
cv2.waitKey(0)
# compute the Scharr gradient of the blackhat image and scale the
# result into the range [0, 255]
gradX = cv2.Sobel(blackhat, ddepth=cv2.CV_32F, dx=1, dy=0, ksize=-1)
gradX = np.absolute(gradX)
(minVal, maxVal) = (np.min(gradX), np.max(gradX))
gradX = (255 * ((gradX - minVal) / (maxVal - minVal))).astype("uint8")

# apply a closing operation using the rectangular kernel to close
# gaps in between letters -- then apply Otsu's thresholding method
gradX = cv2.morphologyEx(gradX, cv2.MORPH_CLOSE, rectKernel)
thresh = cv2.threshold(gradX, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

cv2.imshow("gradXX", gradX)
cv2.waitKey(0)
cv2.imshow("thresh", thresh)
cv2.waitKey(0)

# perform another closing operation, this time using the square
# kernel to close gaps between lines of the MRZ, then perform a
# serieso of erosions to break apart connected components
thresh = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, sqKernel)
thresh = cv2.erode(thresh, None, iterations=2)

# show the output images
cv2.imshow("Image", image)
cv2.waitKey(0)
